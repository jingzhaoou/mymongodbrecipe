include_recipe "mongodb::mongodb_org_repo"

Chef::Log.warn('Jingzhao: using mongodb_org_repo')

node.default['mongodb']['dbconfig_file'] = "/etc/mongod.conf"

include_recipe "mongodb::default"

Chef::Log.warn(node['mongodb']['config']['bind_ip'])
Chef::Log.warn(node['mongodb']['dbconfig_file'])

service "mongod" do
  action :restart
end